apiVersion: v1
kind: Template
labels:
  app: curator
  template: curator-jenkins-pipeline
metadata:
  annotations:
    description: ElasticSearch Curator - Jenkinsfile
    iconClass: icon-openshift
    openshift.io/display-name: ElasticSearch Curator CI
    tags: curator
  name: curator-jenkins-pipeline
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    annotations:
      description: Tests ElasticSearch Curator images
    name: curator-jenkins-pipeline
  spec:
    strategy:
      jenkinsPipelineStrategy:
        jenkinsfile: |-
          def gitCommitMsg = ''
          def templateMark = 'curator-jenkins-ci'
          def templateSel  = 'jenkins-ci-mark'
          pipeline {
              agent {
                  node { label 'maven' }
              }
              options { timeout(time: 70, unit: 'MINUTES') }
              parameters {
                  string(defaultValue: 'master', description: 'Apache Docker Image Tag', name: 'apacheTag')
                  string(defaultValue: 'master', description: 'ElasticSearch Curator Docker Image - Source Git Branch', name: 'buildBranch')
                  string(defaultValue: 'master', description: 'ElasticSearch Curator Docker Image - Source Git Hash', name: 'buildHash')
                  string(defaultValue: '${GIT_SOURCE_HOST}/${GIT_REPOSITORY}', description: 'ElasticSearch Curator Docker Image - Source Git Repository', name: 'buildRepo')
                  string(defaultValue: '3', description: 'Max Retry', name: 'jobMaxRetry')
                  string(defaultValue: '1', description: 'Retry Count', name: 'jobRetryCount')
                  string(defaultValue: '${OPENSHIFT_ROUTED_DOMAIN}', description: 'CI Router Root Domain', name: 'rootDomain')
              }
              stages {
                  stage('pre-cleanup') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      echo "Using project: ${openshift.project()}"
                                      echo "cleaning up previous assets for curator-${params.buildHash}"
                                      openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("configmaps", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("deploymentconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("routes", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("services", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  }
                              }
                          }
                      }
                  }
                  stage('create') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(10) {
                                              def cloneProto = "http"
                                              def created
                                              def objectsFromTemplate
                                              def privateRepo = false
                                              def repoHost = params.buildRepo.split('/')[0]
                                              def templatePath = "/tmp/workspace/${namespace}/${namespace}-curator-jenkins-pipeline/tmpcurator${params.buildBranch}/deploy/openshift"
                                              sh "git config --global http.sslVerify false"
                                              sh "rm -fr tmpcurator${params.buildBranch}; mkdir -p tmpcurator${params.curatorBranch}"
                                              dir ("tmpcurator${params.buildBranch}") {
                                                  try {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          cloneProto = "https"
                                                          privateRepo = true
                                                          echo "cloning ${params.buildRepo} over https, using ${repoHost} token"
                                                          try { git([ branch: "${params.buildBranch}", url: "https://${GIT_TOKEN}@${params.buildRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.buildRepo}#${params.buildBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      }
                                                  } catch(e) {
                                                      if (privateRepo != true) {
                                                          echo "caught ${e} - assuming no credentials required"
                                                          echo "cloning ${params.buildRepo} over http"
                                                          try { git([ branch: "${params.buildBranch}", url: "http://${params.buildRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.buildRepo}#${params.buildBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      } else { throw e }
                                                  }
                                                  try {
                                                      gitCommitMsg = sh(returnStdout: true, script: "git log -n 1").trim()
                                                  } catch(e) { echo "In non-critical catch block resolving commit message - ${e}" }
                                              }
                                              try { sh "test -d ${templatePath}" }
                                              catch (e) {
                                                  echo "Could not find ./deploy/openshift in ${params.buildRepo}#${params.buildBranch}"
                                                  throw e
                                              }
                                              echo "Processing ElasticSearch Curator:${params.buildHash}, from ${repoHost}, tagging to ${params.buildBranch}"
                                              echo "Building from Apache:${params.apacheTag}"
                                              try {
                                                  echo " == Creating ImageStream =="
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/imagestream.yaml")
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating ImageStream - ${e}" }
                                              echo " == Creating BuildConfigs =="
                                              if (privateRepo) {
                                                  withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                      objectsFromTemplate = openshift.process("-f", "${templatePath}/build-with-secret.yaml", '-p', "APACHE_IMAGESTREAM_TAG=apache:${params.apacheTag}",
                                                          '-p', "S3EXPLORER_REPOSITORY_REF=${params.buildHash}", '-p', "S3EXPLORER_REPOSITORY_URL=${cloneProto}://${params.buildRepo}", '-p', "GIT_DEPLOYMENT_TOKEN=${GIT_TOKEN}")
                                                  }
                                              } else {
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/build.yaml", '-p', "APACHE_IMAGESTREAM_TAG=apache:${params.apacheTag}",
                                                      '-p', "S3EXPLORER_REPOSITORY_REF=${params.buildHash}", '-p', "S3EXPLORER_REPOSITORY_URL=${cloneProto}://${params.buildRepo}")
                                              }
                                              echo "The template will create ${objectsFromTemplate.size()} objects"
                                              for (o in objectsFromTemplate) { o.metadata.labels["${templateSel}"] = "${templateMark}-${params.buildHash}" }
                                              created = openshift.apply(objectsFromTemplate)
                                              created.withEach { echo "Created ${it.name()} from template with labels ${it.object().metadata.labels}" }
                                          }
                                      } catch(e) {
                                          echo "In catch block while creating resources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('build') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      try {
                                          timeout(20) {
                                              echo "watching curator-${params.buildHash} docker image build"
                                              def builds = openshift.selector("bc", [ name: "curator-${params.buildHash}" ]).related('builds')
                                              builds.untilEach(1) { return (it.object().status.phase == "Complete") }
                                          }
                                      } catch(e) {
                                          echo "In catch block while building Docker image - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('tag') {
                      steps {
                          script {
                              if ("${params.buildBranch}" == "${params.buildHash}") { echo "skipping tag - source matches target" }
                              else {
                                  openshift.withCluster() {
                                      openshift.withProject() {
                                          try {
                                              timeout(5) {
                                                  def namespace = "${openshift.project()}"
                                                  retry(3) {
                                                      sh """
                                                      oc login https://kubernetes.default.svc.cluster.local --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) > /dev/null 2>&1
                                                      oc tag -n ${namespace} curator:${params.buildHash} curator:${params.buildBranch}
                                                      """
                                                  }
                                              }
                                          } catch(e) {
                                              echo "In catch block while tagging ElasticSearch Curator image - ${e}"
                                              throw e
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              post {
                  always {
                      script {
                          openshift.withCluster() {
                              openshift.withProject() {
                                  def namespace   = "${openshift.project()}"
                                  def postJobName = "${namespace}/${namespace}-post-triggers-jenkins-pipeline"
                                  currentBuild.description = """
                                  ${params.buildRepo} ${params.buildBranch} (try ${params.jobRetryCount}/${params.jobMaxRetry})
                                  ${gitCommitMsg}
                                  """.stripIndent()
                                  echo "cleaning up assets for curator-${params.buildHash}"
                                  sh "rm -fr /tmp/workspace/${namespace}/${namespace}-curator-jenkins-pipeline/tmpcurator${params.buildBranch}"
                                  openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("configmaps", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("deploymentconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("routes", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("services", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  def jobParams = [
                                          [$class: 'StringParameterValue', name: "hasUpstream", value: "yes"],
                                          [$class: 'StringParameterValue', name: "jobMaxRetry", value: params.jobMaxRetry],
                                          [$class: 'StringParameterValue', name: "jobRetryCount", value: params.jobRetryCount],
                                          [$class: 'StringParameterValue', name: "jobStatus", value: currentBuild.currentResult],
                                          [$class: 'StringParameterValue', name: "sourceBranch", value: params.buildBranch],
                                          [$class: 'StringParameterValue', name: "sourceComponent", value: "curator"],
                                          [$class: 'StringParameterValue', name: "sourceRef", value: params.buildHash],
                                          [$class: 'StringParameterValue', name: "sourceRepo", value: params.buildRepo],
                                          [$class: 'StringParameterValue', name: "upstreamSourceBranch", value: params.apacheTag],
                                          [$class: 'StringParameterValue', name: "upstreamSourceComponent", value: "apache"]
                                      ]
                                  try { build job: postJobName, parameters: jobParams, propagate: false, wait: false }
                                  catch(e) { echo "caught ${e} starting Job post-process" }
                              }
                          }
                      }
                  }
                  changed { echo "changed?" }
                  failure { echo "Build failed (${params.jobRetryCount} out of ${params.jobMaxRetry})" }
                  success { echo "success!" }
                  unstable { echo "unstable?" }
              }
          }
      type: JenkinsPipeline
parameters:
- name: GIT_REPOSITORY
  description: Git Repostory URL, Relative to GIT_SOURCE_HOST
  displayName: Git Repository
  value: synacksynack/opsperator/docker-curator.git
- name: GIT_SOURCE_HOST
  description: Git FQDN we would build images from
  displayName: Git Server
  value: gitlab.com
- name: OPENSHIFT_ROUTED_DOMAIN
  description: OpenShift Routers-served Root Domain
  displayName: CI Router Domain
  value: ci.apps.intra.unetresgrossebite.com
