FROM docker.io/alpine:3.6

# ElasticSearch Curator image for OpenShift Origin

ENV CURATOR_VERSION=5.8.1

LABEL io.k8s.description="ElasticSearch Curator $CURATOR_VERSION." \
      io.k8s.display-name="ElasticSearch Curator $CURATOR_VERSION" \
      io.openshift.tags="elasticsearch,curator" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-curator" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$CURATOR_VERSION"

RUN set -x \
    && apk --no-cache add python py-setuptools py-pip gcc libffi py-cffi \
	python-dev libffi-dev py-openssl musl-dev linux-headers openssl-dev \
	libssl1.0 \
    && pip install elasticsearch-curator==$CURATOR_VERSION \
    && pip install boto3==1.9.143 \
    && pip install requests-aws4auth==0.9 \
    && pip install cryptography==2.6.1 \
    && apk del py-pip gcc python-dev libffi-dev musl-dev linux-headers \
	openssl-dev \
    && sed -i '/import sys/a urllib3.contrib.pyopenssl.inject_into_urllib3()' \
	/usr/bin/curator \
    && sed -i '/import sys/a import urllib3.contrib.pyopenssl' \
	/usr/bin/curator \
    && sed -i '/import sys/a import urllib3' /usr/bin/curator

USER nobody:nobody

ENTRYPOINT ["/usr/bin/curator"]
